CREATE TABLE Tb_user (
  id int(11) NOT NULL AUTO_INCREMENT,
  fullname varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  username varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  password varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username (username)
 ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
