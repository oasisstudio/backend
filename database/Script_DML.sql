
INSERT INTO Tb_FeedType(Tb_FeedType.Name_FeedType)VALUES ('Comida 1','Concentrado/Silo/Sacate');
INSERT INTO Tb_FeedType(Tb_FeedType.Name_FeedType)VALUES ('Comida 2','Silo/Sacate');
INSERT INTO Tb_FeedType(Tb_FeedType.Name_FeedType)VALUES ('Comida 3','Silo/Sacate');

INSERT INTO Tb_RationFeed(Tb_RationFeed.Amount_RFeed) VALUES ('28C + 32S + 40S');
INSERT INTO Tb_RationFeed(Tb_RationFeed.Amount_RFeed) VALUES ('15C + 32S + 35S');
INSERT INTO Tb_RationFeed(Tb_RationFeed.Amount_RFeed) VALUES ('6S + 35S');

INSERT INTO Tb_AnimalsGroup (Tb_AnimalsGroup.Name_AnimalG, Tb_AnimalsGroup.Id_FeedType, Tb_AnimalsGroup.Id_RFeed) VALUES ('Producción Alta', 1,1);
INSERT INTO Tb_AnimalsGroup (Tb_AnimalsGroup.Name_AnimalG, Tb_AnimalsGroup.Id_FeedType, Tb_AnimalsGroup.Id_RFeed) VALUES ('Producción Media',2,2);
INSERT INTO Tb_AnimalsGroup (Tb_AnimalsGroup.Name_AnimalG, Tb_AnimalsGroup.Id_FeedType, Tb_AnimalsGroup.Id_RFeed) VALUES ('Producción Baja', 3,3);

INSERT INTO Tb_Breed (Tb_Breed.Name_Breed) VALUES ('Simmental');
INSERT INTO Tb_Breed (Tb_Breed.Name_Breed) VALUES ('Holstein');
INSERT INTO Tb_Breed (Tb_Breed.Name_Breed) VALUES ('Cebú');

INSERT INTO Tb_Animal (Tb_Animal.Name_Animal, Tb_Animal.Birthdate, Tb_Animal.Id_Breed, Tb_Animal.Id_AnimalsG) VALUES ('Menchito','2018-10-10',1,1);


INSERT INTO Tb_ProductionMilk (Tb_ProductionMilk.Amount_Milk, Tb_ProductionMilk.Date_ProductionMilk, Tb_ProductionMilk.Id_Animal) 
VALUES ('50', '2021-10-16',1);

INSERT INTO Tb_ProductionMilk (Tb_ProductionMilk.Amount_Milk, Tb_ProductionMilk.Date_ProductionMilk, Tb_ProductionMilk.Id_Animal) 
VALUES ('100', '2021-10-16',2);

---------------ANIMALS------------------------

DELIMITER //
CREATE PROCEDURE AddAnimal(
IN _id INT,
IN _Name VARCHAR(50),
IN _Birthdate VARCHAR(50),
IN _Breed INT,
IN _AnimalsG INT)

BEGIN 
		IF _id = 0 THEN
        	INSERT INTO Tb_Animal (Name_Animal, Birthdate, Id_Breed, Id_AnimalsG)
            VALUES(_Name, _Birthdate, _Breed, _AnimalsG);
            
			SET _id = LAST_INSERT_ID();
        ELSE
        	UPDATE Tb_Animal
            
            SET
            	Name_Animal = _Name,
            	Birthdate   = _Birthdate,
            	Id_Breed    = _Breed,
            	Id_AnimalsG = _AnimalsG
            
            WHERE Id_Animal = _id;
         END IF;
         
            SELECT _id AS id;

END//

DELIMITER ;

-----------------MILK --------------------------

DELIMITER //
CREATE PROCEDURE AddMilk(
IN _id INT,
IN _Amount_Milk VARCHAR(50),
IN _Date_ProductionMilk VARCHAR(50),
IN _Id_Animal INT)

BEGIN 
		IF _id = 0 THEN
        	INSERT INTO Tb_ProductionMilk (Amount_Milk, Date_ProductionMilk, Id_Animal)
            VALUES(_Amount_Milk, _Date_ProductionMilk,  _Id_Animal);
            
			SET _id = LAST_INSERT_ID();
        ELSE
        	UPDATE Tb_ProductionMilk
            
            SET
            	Amount_Milk			   = _Amount_Milk,
            	Date_ProductionMilk    = _Date_ProductionMilk,
            	Id_Animal 			   = _Id_Animal
            
            WHERE Id_ProductionMilk = _id;
         END IF;
         
            SELECT _id AS id;

END//

DELIMITER ;