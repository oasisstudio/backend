const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
app.use(cors());
app.set('port', process.env.PORT || 3000);


app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//settings


//global variable
app.use((req, res, next) => {

    next();
});

//rutas
app.use(require('./routes'));


app.listen(app.get('port'), () => {
    console.log('server on port', app.get('port'));

});