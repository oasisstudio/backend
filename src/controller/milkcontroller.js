const pool = require('../model/dbConnection');


exports.setmilk = (req, res) => {

    pool.query(`    
    SELECT 
    PM.Id_ProductionMilk,
    A.Name_Animal,
    DATE_FORMAT(PM.Date_ProductionMilk, '%d-%m-%Y') AS Date_ProductionMilk,
    PM.Amount_Milk,
    A.Id_Animal
    
    FROM  Tb_ProductionMilk AS PM, Tb_Animal AS A
    WHERE  PM.Id_Animal = A.Id_Animal`, 
      
      function (err, rows) {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

}

exports.insertMilk = (req, res) => {

    const { _id, _Amount_Milk, _Date_ProductionMilk, _Id_Animal } = req.body;

    const NewMilk = `CALL AddMilk(?,?,?,?);`;

    pool.query(NewMilk, [_id, _Amount_Milk, _Date_ProductionMilk, _Id_Animal], (err, rows) => {

        if (!err) {

            res.json({ Status: 'Aguardado' });
        } else {
            console.log(err);
        }
    });

}

