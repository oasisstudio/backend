const express = require('express');
const pool = require('../model/dbConnection');


exports.Animals = (req, res) => {

    pool.query(` 
    select 
    A.Id_Animal,
    A.Name_Animal,  
    DATE_FORMAT( A.Birthdate, '%d-%m-%Y') AS Birthdate,
    B.Name_Breed,
    G.Name_AnimalG,
    FT.Name_FeedType,
    FT.FeedType_Description,
    Rf.Amount_RFeed
    FROM Tb_Animal A, Tb_Breed B, Tb_AnimalsGroup G, Tb_RationFeed Rf, Tb_FeedType FT
    WHERE A.Id_Breed = B.Id_Breed AND A.Id_AnimalsG = G.Id_AnimalsG AND G.Id_RFeed = Rf.Id_RFeed AND G.Id_FeedType = FT.Id_FeedType
    
    `, function (err, rows) {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

}

exports.AmoutAnimals = (req, res) => {

    pool.query(` 
    SELECT 
    DATE_FORMAT(Date_ProductionMilk, '%d-%m-%Y') AS Date_ProductionMilk, 
    Amount_Milk
    FROM 
    Tb_ProductionMilk 
    
    
    `, function (err, rows) {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

}

exports.TotalMilk = (req, res) => {

    pool.query(` 
    select 
    sum(Amount_Milk) as cantidad
    from Tb_ProductionMilk 
    
    `, function (err, rows) {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

}

exports.TotalAnimals = (req, res) => {

    pool.query(` 
    SELECT 
    COUNT((length(A.Id_Animal))) AS TotalAnimals
    FROM Tb_Animal A
    `, function (err, rows) {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

}


exports.InsertAnimals = (req, res) => {

    const { _id, _NameAnimals, _Birthdate, _Breed, _AnimalsG } = req.body;
    console.log(req.body);

    const NewAnimal = `CALL AddAnimal(?,?,?,?,?);`;

    pool.query(NewAnimal, [_id, _NameAnimals, _Birthdate, _Breed, _AnimalsG], (err, rows) => {

        if (!err) {

            res.json({ Status: 'Aguardado' });
        } else {
            console.log(err);
        }
    });

}


