const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const conn = require('../model/dbConnection').promise();


exports.register = async(req,res,next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return res.status(422).json({ errors: errors.array() });
    }

    try{

        const [row] = await conn.execute(
            "SELECT `username` FROM `Tb_user` WHERE `username`=?",
            [req.body.username]
          );

        if (row.length > 0) {
            return res.status(201).json({
                message: "The username already in use",
            });
        }

        const hashPass = await bcrypt.hash(req.body.password, 12);

        const [rows] = await conn.execute('INSERT INTO `Tb_user`(`fullname`,`username`,`password`) VALUES(?,?,?)',[
            req.body.fullname,
            req.body.username,
            hashPass
        ]);

        if (rows.affectedRows === 1) {
            return res.status(201).json({
                message: "The user has been successfully inserted.",
                
            });
        }
        console.log(this.register);
    }catch(err){
        next(err);
    }
}