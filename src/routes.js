const express = require('express');
const router = express.Router();
const { body } = require('express-validator');
const pool = require('./model/dbConnection');


const { register } = require('./controller/registercontroller');
const { login } = require('./controller/logincontroller');
const { Animals } = require('./controller/animalscontroller');
const { AmoutAnimals } = require('./controller/animalscontroller');
const { TotalAnimals } = require('./controller/animalscontroller');
const { TotalMilk } = require('./controller/animalscontroller');
const { InsertAnimals } = require('./controller/animalscontroller');
const { setmilk } = require('./controller/milkcontroller');
const { insertMilk } = require('./controller/milkcontroller');


router.post('/register', [
    body('fullname', "El nombre tiene que tener almenos tres caracteristicas")
        .notEmpty()
        .trim()
        .isLength({ min: 3 }),
    body('username', "Invalido")
        .notEmpty()
        .trim(),
    body('password', "The Password must be of minimum 4 characters length").notEmpty().trim().isLength({ min: 4 }),
], register);


router.post('/login', [
    body('username', "Usuarios invalido")
        .notEmpty()
        .trim(),
    body('password', "The Password must be of minimum 4 characters length").notEmpty().trim().isLength({ min: 4 }),
], login);


router.post('/signup', (req, res) => {

    console.log(req.body)
    res.send('Recibido');

});

router.get('/animals', Animals);
router.get('/amount', AmoutAnimals);
router.get('/totalMilk', TotalMilk);
router.get('/totalAnimals', TotalAnimals);
router.post('/insertAnimals', InsertAnimals);


router.get('/setmilk', setmilk);
router.post('/insertmilk', insertMilk);


router.get('/:id', (req, res) => {

    const { id } = req.params;
    const UpdateAnimal = ` 
    
    SELECT
    A.Id_Animal,
    A.Name_Animal,
    DATE_FORMAT(A.Birthdate, '%Y-%m-%d') AS Birthdate,
    A.Id_Breed,
    A.Id_AnimalsG
    FROM Tb_Animal AS A
    WHERE A.Id_Animal = ?`

    pool.query(UpdateAnimal, [id], (err, rows, filds) => {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

});


router.get('/deleteAnimals/:id', (req, res) => {

    const { id } = req.params;
    const DeleteAnimal = ` 
    
    DELETE FROM Tb_Animal WHERE Tb_Animal.Id_Animal = ?`

    pool.query(DeleteAnimal, [id], (err, rows, filds) => {

        if (!err) {

            res.json({ Status: 'Eliminado' });
        } else {
            console.log(err);
        }
    });

});

router.put('/UpdateAnimal:_id', (req, res) => {

    const { _NameAnimals, _Birthdate, _Breed, _AnimalsG } = req.body;
    const { _id } = req.params;
    const UpdateAnimal = ` CALL AddAnimal(?,?,?,?,?)`;
    console.log(_id);
    pool.query(UpdateAnimal, [_id, _NameAnimals, _Birthdate, _Breed, _AnimalsG], (err, rows, filds) => {

        if (!err) {

            res.json({ Status: 'Actualizado' });
        } else {
            console.log(err);
        }
    });

});


router.get('/milk/:id', (req, res) => {

    const { id } = req.params;
    const UpdateMilk = `

    SELECT 
    PM.Id_ProductionMilk,
    A.Name_Animal,
    DATE_FORMAT(PM.Date_ProductionMilk, '%Y-%m-%d') AS Date_Milk,
    PM.Amount_Milk,
    A.Id_Animal
    
    FROM  Tb_ProductionMilk AS PM 
    INNER JOIN Tb_Animal AS A ON PM.Id_Animal = A.Id_Animal
   WHERE Id_ProductionMilk = ?`

    pool.query(UpdateMilk, [id], (err, rows, filds) => {

        if (!err) {

            res.json(rows);
        } else {
            console.log(err);
        }
    });

});

router.put('/UpdateMilk/:_id', (req, res) => {


    const {_Amount_Milk, _Date_ProductionMilk, _Id_Animal } = req.body;
    const { _id } = req.params;
    const NewMilk = `CALL AddMilk(?,?,?,?);`;

    pool.query(NewMilk, [_id, _Amount_Milk, _Date_ProductionMilk, _Id_Animal], (err, rows) => {

        if (!err) {

            res.json({ Status: 'Actulizado' });
        } else {
            console.log(err);
        }
    });

});


router.get('/deleteMilk/:id', (req, res) => {

    const { id } = req.params;
    const DeleteAnimal = ` 
    
    DELETE FROM Tb_ProductionMilk WHERE Tb_ProductionMilk.Id_ProductionMilk = ?`

    pool.query(DeleteAnimal, [id], (err, rows, filds) => {

        if (!err) {

            res.json({ Status: 'Eliminado' });
        } else {
            console.log(err);
        }
    });

});

module.exports = router;