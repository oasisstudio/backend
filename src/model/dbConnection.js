const mysql = require("mysql2");

const { database } = require('./keys');

const db_connection = mysql
  .createConnection(database)
  .on("error", (err) => {
    console.log("Error database ", err);
  });
  console.log("Database Conect")

module.exports = db_connection;